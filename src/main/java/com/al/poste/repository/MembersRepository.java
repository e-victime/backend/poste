package com.al.poste.repository;

import com.al.poste.model.Equipe;
import com.al.poste.model.Members;
import com.al.poste.model.MembersKey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MembersRepository extends JpaRepository<Members, MembersKey>, JpaSpecificationExecutor<Members> {
    Page<Members> getMembersByEquipe(Equipe equipe, Pageable pageable);
    Members getMembersByEquipe_IdAndPersonnel_Id(Integer equipe_id, Integer personnel_id);

}
