package com.al.poste.repository;

import com.al.poste.model.Equipe;
import com.al.poste.model.Personnel;
import com.al.poste.model.Poste;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

@Repository
@Table("equipe")
public interface EquipeRepository extends JpaRepository<Equipe, Integer>, JpaSpecificationExecutor<Equipe> {

    Equipe findEquipeById(Integer id);

    Page<Equipe> findEquipesByChefEquipe(Personnel chefEquipe, Pageable pageable);

//    @Query(value = "SELECT e FROM Equipe e INNER JOIN Poste p ON e.poste = p WHERE p.id = :poste_id ORDER BY p.id")
//    Page<Equipe> findequipesByPosteId(@Param("poste_id") Integer poste_id, Pageable pageable);
    Page<Equipe> getEquipesByPoste(Poste poste, Pageable pageable);

//    @Query(value = "SELECT e FROM Equipe e INNER JOIN Poste p ON e.poste = p WHERE p.id = :poste_id AND e.binomeEquipe = :binomeEquipe ORDER BY p.id")
//    Page<Equipe> getEquipesByBinomeEquipeAndPosteId(@Param("binomeEquipe") Boolean binomeEquipe, @Param("poste_id") Integer poste_id, Pageable pageable);
    Page<Equipe> getEquipesByBinomeEquipeAndPoste(@NonNull Boolean binomeEquipe, Poste poste, Pageable pageable);

//    @Query(value = "SELECT e FROM Equipe e INNER JOIN Poste p ON e.poste = p WHERE p.id = :poste_id AND e.nom LIKE %:nom% ORDER BY p.id")
//    Page<Equipe> findEquipesByNomAndCentreId(@Param("nom") String nom, @Param("centre_id") Integer centre_id, Pageable pageable);

    @Query(value = "SELECT e.* FROM equipe e INNER JOIN members m on e.id = m.equipe_id INNER JOIN personnel p on m.personnel_id = p.id WHERE p.id = :idPersonnel ORDER BY e.id \\n#pageable\\n",
            countQuery = "SELECT count(*) FROM equipe e INNER JOIN members m on e.id = m.equipe_id INNER JOIN personnel p on m.personnel_id = p.id WHERE p.id = :idPersonnel",
            nativeQuery = true)
    Equipe getEquipeByPersonnel(Integer idPersonnel);

}
