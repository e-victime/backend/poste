package com.al.poste.repository;

import com.al.poste.model.Personnel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.relational.core.mapping.Table;


@Repository
@Table("personnel")
public interface PersonnelRepository  extends JpaRepository<Personnel, Integer>, JpaSpecificationExecutor<Personnel> {

    @Query(value = "SELECT p.* FROM personnel p JOIN members m on p.id = m.personnel_id JOIN equipe e on m.equipe_id = e.id WHERE e.id = :idEquipe ORDER BY p.id \\n#pageable\\n",
            countQuery = "SELECT count(*) FROM personnel p JOIN members m on p.id = m.personnel_id JOIN equipe e on m.equipe_id = e.id WHERE e.id = :idEquipe",
            nativeQuery = true)
    Page<Personnel> getPersonnelsByEquipe(@Param("idEquipe") Integer idEquipe, Pageable pageable);



}
