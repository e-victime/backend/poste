package com.al.poste.controller;

import com.al.poste.model.*;
import com.al.poste.service.EquipeService;
import com.al.poste.service.PosteService;
import com.sipios.springsearch.anotation.SearchSpec;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * Controller du micro-service Poste
 *
 * @author Antoine Thys
 */

@RestController
@RequestMapping(value = "/poste")
public class PosteController {

    @Autowired
    @Resource
    PosteService PosteService;

    @Autowired
    @Resource
    EquipeService EquipeService;

    // POSTE

    /**
     * Récupération de tous les Postes
     *
     * @return List de tous les Postes
     */
    @GetMapping
    public Page<Poste> getAllPoste(Pageable pageable) {
        return PosteService.getAllPoste(pageable);
    }

    /**
     * Récupération d’un Poste
     *
     * @param id Identifiant d’un Poste
     * @return Poste spécifié
     */
    @GetMapping(value = "/{id}")
    public Poste getPosteById(@PathVariable(value = "id") Integer id) {
        return PosteService.getPosteById(id);
    }

    /**
     * Recherche d’un Poste par nom d’utilisateur
     *
     * @param name String : Nom du poste pour la recherche
     * @return Page : Page de Postes
     * @deprecated : Remplacé par le moteur de recherche Sipios
     * @see PosteController#searchPoste(Specification, Pageable)
     */
    @GetMapping(value = "/search/{name}")
    public Page<Poste> getPosteByUsername(@PathVariable(value = "name") String name, Pageable pageable) {
        return PosteService.getPosteByNom(name, pageable);
    }

    /**
     * Création de Poste
     *
     * @param Poste Poste : Poste a créé
     * @return Poste + HttpStatus : Poste créé && Status Http "CREATED"
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Optional<Poste>> createPoste(@RequestBody Poste Poste) {
        Optional<Poste> c = PosteService.createPoste(Poste);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    /**
     * Mise à jour d’un Poste
     *
     * @param Poste Poste : Nouvelles informations du Poste
     * @param id    Integer : ID du Poste à modifier
     * @return Poste : Poste modifié && Status Http "OK"
     */
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Poste> updatePostePut(@PathVariable("id") Integer id, @RequestBody Poste Poste) {
        Poste.setId(id);
        Poste u = PosteService.updatePostePut(Poste);
        return new ResponseEntity<>(u, HttpStatus.OK);
    }

    /**
     * Suppression d’un Poste
     *
     * @param id Integer : ID du Poste à supprimer
     * @return HttpStatus : Status Http "OK"
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HttpEntity<HttpStatus> deletePoste(@PathVariable("id") Integer id) {
        String status = PosteService.deletePoste(id);
        if (status.equals("OK")) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    // Equipe

    /**
     * Récupération de toutes les Equipes d’un Poste
     *
     * @param idPoste  Integer : id du Poste
     * @param pageable Pageable : pagination
     * @return Page : Equipe
     */
    @GetMapping(value = "/{idPoste}/equipe")
    public Page<Equipe> getEquipesOnPoste(@PathVariable("idPoste") Integer idPoste, Pageable pageable) {
        return EquipeService.getAllEquipesOnPoste(idPoste, pageable);
    }

    /**
     * Récupération d’une Equipe
     *
     * @param idPoste  Integer : id du Poste passé dans l’URL
     * @param idEquipe Integer : id de l’Equipe
     * @return Equipe
     */
    @GetMapping(value = "/{idPoste}/equipe/{idEquipe}")
    public Equipe getEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe) {
        return EquipeService.getEquipeById(idEquipe);
    }

    /**
     * Création d’une Equipe
     *
     * @param idPoste  Integer : id du Poste
     * @param equipe   Equipe : equipe à créer
     * @return Equipe + HttpStatus.CREATED : Nouvelle Equipe && status http
     */
    @PostMapping(value = "/{idPoste}/equipe")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Equipe> createEquipe(@PathVariable("idPoste") Integer idPoste, @RequestBody Equipe equipe) {
        Equipe e = EquipeService.createEquipe(equipe, idPoste);
        return new ResponseEntity<>(e, HttpStatus.CREATED);
    }

    /**
     * Définition du Chef d’Equipe
     *
     * @param idPoste  Integer : id du Poste
     * @param idEquipe Integer : id de l’Equipe passé dans l’URL
     * @param idChef   Integer : id de la personne à nommer Chef d’Equipe
     * @return HttpStatus : OK si correctement effectué
     */
    @PostMapping(value = "/{idPoste}equipe/{idEquipe}/defChef")
    @ResponseStatus(HttpStatus.OK)
    public HttpStatus defineChefEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, @RequestParam Integer idChef) {
        return EquipeService.defineChefEquipe(idEquipe, idChef);
    }

    /**
     * Mise à jour d’une Equipe
     *
     * @param equipe   Equipe : Nouvelles informations de l’Equipe
     * @param idPoste  Integer : ID du Poste
     * @param idEquipe Integer : ID Equipe à modifier
     * @return Poste : Poste modifié && Status Http "OK"
     */
    @PutMapping(value = "/{idPoste}/equipe/{idEquipe}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Equipe> updateEquipePut(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, @RequestBody Equipe equipe) {
        equipe.setId(idEquipe);
        equipe.setPoste(PosteService.getPosteById(idPoste));
        Equipe e = EquipeService.updateEquipePut(equipe);
        return new ResponseEntity<>(e, HttpStatus.OK);
    }

    /**
     * Suppression d’une Equipe
     *
     * @param idPoste  Integer : id du Poste dans l’URL
     * @param idEquipe Integer : id de l’Equipe dans l’URL
     * @return HttpStatus : HttpStatus Ok ou Bad Request
     */
    @DeleteMapping(value = "/{idPoste}equipe/{idEquipe}")
    @ResponseStatus(HttpStatus.OK)
    public HttpEntity<HttpStatus> deleteEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe) {
        String status = EquipeService.deleteEquipe(idEquipe);
        if (status.equals("OK")) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Récupération des Personnels dans une Equipe
     *
     * @param idPoste  Integer : id du Poste (non utilisé)
     * @param idEquipe Integer : id de l’Equipe où chercher les utilisateurs
     * @param pageable Pageable : pageable
     * @return List : Personnels
     */
    @GetMapping(value = "/{idPoste}/equipe/{idEquipe}/personnel")
    public Page<Personnel> getPersonnelOnEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, Pageable pageable) throws NotFoundException {
        return EquipeService.getMembresEquipe(idEquipe, pageable);
    }

    /**
     * Récupération de l’id radio d’une Equipe
     * @param idPoste id du Poste (non utilisé)
     * @param idEquipe id de l’Equipe
     * @return idRadio + Http Status
     */
    @GetMapping(value = "/{idPoste}/equipe/{idEquipe}/idRadio")
    public ResponseEntity<String> getEquipeIdRadio(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe) {
        String s = EquipeService.getEquipeById(idEquipe).getIdentifiantRadioEquipe();
        return new ResponseEntity<>(s,HttpStatus.OK);
    }

    /**
     * Récupération de l’id radio de toutes les Equipes
     * @param idPoste id du Poste
     * @return Page id radio
     */
    @GetMapping(value = "/{idPoste}/equipe/idRadio")
    public ResponseEntity<Page<String>> getEquipesIdRadio(@PathVariable("idPoste") Integer idPoste, Pageable pageable) {
        Page<String> s = EquipeService.getIdRadioAllEquipes(idPoste, pageable);
        return new ResponseEntity<>(s, HttpStatus.OK);
    }

    /**
     * Ajout d’un Personnel à une Equipe
     *
     * @param idPoste     Integer : id du Poste (non utilisé)
     * @param idEquipe    Integer : id de l’Equipe
     * @param personnelToEquipeForm Object : Integer + String pour idPersonnel et Role
     * @return HttpStatus : CREATED or BAD_REQUEST
     */
    @PostMapping(value = "/{idPoste}/equipe/{idEquipe}/personnel")
    @ResponseStatus(HttpStatus.CREATED)
    public HttpEntity<HttpStatus> createEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, @RequestBody @Validated PersonnelToEquipeForm personnelToEquipeForm) {
        Integer idPersonnel = personnelToEquipeForm.getIdPersonnel();
        String role = personnelToEquipeForm.getRole();
        Boolean status = EquipeService.addPersonnelToEquipe(idEquipe, idPersonnel, role);
        if (status) return new ResponseEntity<>(HttpStatus.CREATED);
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * @param idPoste     Integer : id du Poste (non utilisé)
     * @param idEquipe    Integer : id d
     * @param idPersonnel Integer : id d
     * @return HttpStatus : OK or BAD_REQUEST
     */
    @DeleteMapping(value = "/{idPoste}/equipe/{idEquipe}/personnel/{idPersonnel}")
    @ResponseStatus(HttpStatus.OK)
    public HttpEntity<HttpStatus> deletePersonnelOnEquipe(@PathVariable("idPoste") Integer idPoste, @PathVariable("idEquipe") Integer idEquipe, @PathVariable("idPersonnel") Integer idPersonnel) {
        Boolean status = EquipeService.deletePersonnelFromEquipe(idEquipe, idPersonnel);
        if (status) return new ResponseEntity<>(HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Changement du Responsable de Poste
     *
     * @param idPoste       Integer : id du Poste passé dans l’URL
     * @param idResponsable Integer : id du Personnel à nommer Responsable du Poste
     * @return HttpStatus : OK si correctement effectué
     */
    @PostMapping(value = "/{idPoste}/responsable")
    @ResponseStatus(HttpStatus.OK)
    public HttpStatus changeResponsable(@PathVariable("idPoste") Integer idPoste, @RequestParam Integer idResponsable) {
        return PosteService.changeResponsable(idPoste, idResponsable);
    }

    /**
     * Changement du Responsable de Poste
     * @param idPoste id du Poste passé dans l’URL
     * @return Personnel + HttpStatus
     */
    @GetMapping(value = "/{idPoste}/responsable")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Optional<Personnel>> getResponsable(@PathVariable("idPoste") Integer idPoste) {
        Optional<Personnel> p = PosteService.getResponsable(idPoste);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    // Méthodes de recherches

    /**
     * Recherche d'un Poste
     * @param specs Spécification de la recherche
     * @param pageable Pagination
     * @return Page de Poste
     */
    @GetMapping(value = "/search/poste")
    public ResponseEntity<Page<Poste>> searchPoste(@SearchSpec Specification<Poste> specs, Pageable pageable) {
        return new ResponseEntity<>(PosteService.searchPoste(specs, pageable), HttpStatus.OK);
    }

    /**
     * Recherche d'une Equipe
     * @param specs Spécification de la recherche
     * @param pageable Pagination
     * @return Page d'Equipes
     */
    @GetMapping(value = "/search/equipe")
    public ResponseEntity<Page<Equipe>> searchEquipe(@SearchSpec Specification<Equipe> specs, Pageable pageable) {
        return new ResponseEntity<>(EquipeService.searchEquipe(specs, pageable), HttpStatus.OK);
    }

    /**
     * Recherche d'un Membre d'une Equipe
     * @param specs Spécification de la recherche
     * @param pageable Pagination
     * @return Page de Members
     */
    @GetMapping(value = "/search/membres")
    public ResponseEntity<Page<Members>> searchMembres(@SearchSpec Specification<Members> specs, Pageable pageable) {
        return new ResponseEntity<>(EquipeService.searchMembres(specs, pageable), HttpStatus.OK);
    }
}
