package com.al.poste.service;

import com.al.poste.model.Equipe;
import com.al.poste.model.Members;
import com.al.poste.model.Personnel;
import javassist.NotFoundException;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public interface EquipeService {

    /**
     * Récupération des Equipes d’un Poste
     * @param idPoste : id du Poste
     * @param pageable : pagination
     * @return : Equipes
     */
    Page<Equipe> getAllEquipesOnPoste(Integer idPoste, Pageable pageable);

    /**
     * Récupération d’une Equipe
     * @param id : id Equipe
     * @return : Equipe
     */
    Equipe getEquipeById(Integer id);

    /**
     * Mise à jour Equipe
     * @param equipe : Equipe à mettre à jour
     * @return : Equipe mise à jour
     */
    Equipe updateEquipePut(Equipe equipe);

    /**
     * Suppression d’une Equipe
     * @param id : id de l’Equipe à supprimer
     * @return : Réponse
     */
    String deleteEquipe(Integer id);

    /**
     * Récupération des membres d’une équipe
     * @param id : id Equipe
     * @param pageable : pagination
     * @return : Membres Equipe
     */
    Page<@NonNull Personnel> getMembresEquipe(Integer id, Pageable pageable) throws NotFoundException;

    /**
     * @param id : id Equipe
     * @return : Identifiant Radio
     */
    String getIdRadioEquipe(Integer id);

    /**
     * Récupération de tous les id radio de toutes les Equipes du Poste
     * @param idPoste id du Poste
     * @param pageable Pagination
     * @return Page de String
     */
    Page<String> getIdRadioAllEquipes(Integer idPoste, Pageable pageable);

    /**
     * Ajout d'un Personnel dans Equipe
     * @param idEquipe : id Equipe où ajouter le Personnel
     * @param idPersonnel : id du Personnel à ajouter
     * @return boolean : booléen réussi ou non
     */
    Boolean addPersonnelToEquipe(Integer idEquipe, Integer idPersonnel, String role);

    /**
     * @param idEquipe Integer : id de l’Equipe
     * @param idPersonnel Integer : id du Personnel
     * @return boolean : booléen réussi ou non
     */
    Boolean deletePersonnelFromEquipe(Integer idEquipe, Integer idPersonnel);

    /**
     * Modification du rôle d’un Personnel dans une Equipe
     * @param role String : role à modifier
     * @param idPersonnel Integer : id du Personnel
     * @return Members : Membre mis à jour
     */
    Members updateMemberRole(String role, Integer idEquipe, Integer idPersonnel);

    /**
     * Recherche d’un Membre
     * @param specs spécification recherche
     * @param pageable pagination
     * @return Page d’un Membre
     */
    Page<Members> searchMembres(Specification<Members> specs, Pageable pageable);

    /**
     * Récupération des binômes ou equipes sur le Poste
     * @param equipe Bool : true for Equipe or false for Binome
     * @param pageable : Pagination
     * @return Page : Equipe
     */
    Page<Equipe> getEquipeByBinomeEquipe(Boolean equipe, Integer idPoste ,Pageable pageable);

    /**
     * Création d’une Equipe
     * @param equipe : Equipe à créer
     * @param idPoste Integer : id de Poste
     * @return Equipe : nouvelle Equipe
     */
    Equipe createEquipe(Equipe equipe, Integer idPoste);

    /**
     * Définition du Chef d’Equipe
     * @param idEquipe Integer : id de l’Equipe
     * @param idChef Integer : id du Personnel à ajouter comme Chef d’Equipe
     * @return HttpStatus : OK si correctement effectué
     */
    HttpStatus defineChefEquipe(Integer idEquipe, Integer idChef);

    /**
     * Recherche d’une Equipe
     * @param specs spécification recherche
     * @param pageable pagination
     * @return Page d’Equipe
     */
    Page<Equipe> searchEquipe(Specification<Equipe> specs, Pageable pageable);
}
