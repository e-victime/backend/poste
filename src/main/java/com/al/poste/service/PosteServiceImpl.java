package com.al.poste.service;

import com.al.poste.model.Personnel;
import com.al.poste.model.Poste;
import com.al.poste.repository.PersonnelRepository;
import com.al.poste.repository.PosteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * Implémentation du Service Poste
 * @author Antoine Thys
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
public class PosteServiceImpl implements PosteService{
    @Autowired
    @Resource
    PosteRepository posteRepository;

    @Autowired
    @Resource
    PersonnelRepository personnelRepository;

    /**
     * @return List de tous les Postes
     */
    @Override
    public Page<Poste> getAllPoste(Pageable pageable) {
        return posteRepository.findAll(pageable);
    }

    /**
     * Trouver un Poste avec son ID
     * @param posteId ID d’un Poste
     * @return Poste
     */
    @Override
    public Poste getPosteById(Integer posteId) {
        if (!posteRepository.existsById(posteId)) {
            throw new ResourceNotFoundException("Poste avec l’ID " + posteId + " non trouvé.");
        }
        return posteRepository.findPosteById(posteId);
    }

    @Override
    public HttpStatus changeResponsable(Integer posteId, Integer responsableId) {

        Optional<Personnel> responsable = personnelRepository.findById(responsableId);
        if (responsable.isPresent()) {
            Poste p = posteRepository.getOne(posteId);
            p.setResponsable(responsable.get());
            posteRepository.save(p);
            return HttpStatus.OK;
        } else
            return HttpStatus.BAD_REQUEST;
    }

    @Override
    public Optional<Personnel> getResponsable(Integer posteId) {
        Optional<Poste> p = posteRepository.findById(posteId);
        return p.flatMap(poste -> personnelRepository.findById(poste.getResponsable().getId()));
    }

    /**
     * Recherche d’un Poste
     *
     * @param specs    spécification recherche
     * @param pageable pagination
     * @return Page d’un Poste
     */
    @Override
    public Page<Poste> searchPoste(Specification<Poste> specs, Pageable pageable) {
        return posteRepository.findAll(Specification.where(specs), pageable);
    }

    /**
     * Création d’un Poste
     * @param Poste Poste
     * @return Poste créé
     */
    @Override
    public Optional<Poste> createPoste(Poste Poste) {
        Integer p = posteRepository.save(Poste).getId();
        return posteRepository.findById(p);
    }

    /**
     * Trouver un Poste par nom d’utilisateur
     * @param name Nom du poste à rechercher
     * @return Liste de Postes
     */
    @Override
    public Page<Poste> getPosteByNom(String name, Pageable pageable) {
        try {
            return posteRepository.findPostesByNom(name, pageable);
        } catch (Exception exc) {
            System.err.println(exc.getMessage());
        }
        return null;
    }

    /**
     * Mise à jour d’un Poste
     * @param Poste Poste à mettre à jour
     * @return Poste avec mis à jour
     */
    @Override
    public Poste updatePostePut(Poste Poste) {
        return posteRepository.save(Poste);
    }

    /**
     * Suppression d’un Poste
     * @param id Poste à supprimer
     * @return String : status
     */
    @Override
    public String deletePoste(Integer id) {
        if (posteRepository.findById(id).isPresent()) {
            try {
                posteRepository.deleteById(id);
                return "OK";
            } catch (Exception exc) {
                return exc.getMessage();
            }
        }
        return "Erreur de suppression du Poste";
    }

}
