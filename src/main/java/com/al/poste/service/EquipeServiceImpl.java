package com.al.poste.service;

import com.al.poste.model.*;
import com.al.poste.repository.EquipeRepository;
import com.al.poste.repository.MembersRepository;
import com.al.poste.repository.PersonnelRepository;
import com.al.poste.repository.PosteRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * Implémentation du Service Equipe
 *
 * @author Antoine Thys
 */
@Service
public class EquipeServiceImpl implements EquipeService {

    @Autowired
    @Resource
    EquipeRepository equipeRepository;

    @Autowired
    @Resource
    PosteRepository posteRepository;

    @Autowired
    @Resource
    PersonnelRepository personnelRepository;

    @Autowired
    @Resource
    MembersRepository membersRepository;

    /**
     * Récupération des Equipes d'un Poste
     *
     * @param idPoste  : id du Poste
     * @param pageable : pagination
     * @return : Equipes
     */
    @Override
    public Page<Equipe> getAllEquipesOnPoste(Integer idPoste, Pageable pageable) {
        Poste p = posteRepository.getOne(idPoste);
        return equipeRepository.getEquipesByPoste(p, pageable);
    }

    /**
     * Récupération d'une Equipe
     *
     * @param id : id Equipe
     * @return : Equipe
     */
    @Override
    public Equipe getEquipeById(Integer id) {
        return equipeRepository.getOne(id);
    }

    /**
     * Mise à jour Equipe
     *
     * @param equipe : Equipe à mettre à jour
     * @return : Equipe mise à jour
     */
    @Override
    public Equipe updateEquipePut(Equipe equipe) {
        return equipeRepository.save(equipe);
    }

    /**
     * Suppression d'une Equipe
     *
     * @param id : id de l'Equipe à supprimer
     * @return : Réponse
     */
    @Override
    public String deleteEquipe(Integer id) {
        if (equipeRepository.findById(id).isPresent()) {
            try {
                equipeRepository.deleteById(id);
                return "OK";
            } catch (Exception exc) {
                return exc.getMessage();
            }
        }
        return "Erreur de suppression de l’Equipe";
    }

    /**
     * @param id       : id Equipe
     * @param pageable : pagination
     * @return : Membres Equipe
     */
    @Override
    public Page<Personnel> getMembresEquipe(Integer id, Pageable pageable) throws NotFoundException {

//        List<Personnel> res = new ArrayList<>();
//        for (Members personnel : membersRepository.getMembersByEquipe_Id(id, pageable).getContent()) {
//            res.add(personnelRepository.getOne(personnel.getId().getPersonnelId()));
//        }
//        return res;
        if (equipeRepository.findById(id).isPresent()) {
            Page<Members> m = membersRepository.getMembersByEquipe(equipeRepository.getOne(id), pageable);
            return m.map(Members::getPersonnel);
        } else {
            throw new NotFoundException("L'Equipe avec l'id " + id + " est introuvable ou est vide.");
        }
    }

    /**
     * @param id : id Equipe
     * @return : Identifiant Radio
     */
    @Override
    public String getIdRadioEquipe(Integer id) {

        return equipeRepository.getOne(id).getIdentifiantRadioEquipe();
    }

    /**
     * @param idPoste  id du Poste
     * @param pageable Pagination
     * @return Page id Radio
     */
    @Override
    public Page<String> getIdRadioAllEquipes(Integer idPoste, Pageable pageable) {
        return equipeRepository.getEquipesByPoste(posteRepository.getOne(idPoste), pageable).map(Equipe::getIdentifiantRadioEquipe);
    }

    /**
     * Ajout d'un Personnel dans Equipe
     *
     * @param idEquipe    : id Equipe où ajouter le Personnel
     * @param idPersonnel : id du Personnel à ajouter
     * @return : Equipe avec personnel ajouté
     */
    @Override
    public Boolean addPersonnelToEquipe(Integer idEquipe, Integer idPersonnel, String role) {
        try {
            Personnel p = personnelRepository.getOne(idPersonnel);
            Equipe e = equipeRepository.getOne(idEquipe);
            MembersKey k = new MembersKey(idEquipe, idPersonnel);
            Members m = membersRepository.save(new Members(k, e, p, role));
            return true;
        } catch (Exception exc) {
            System.err.println(exc.getMessage());
            return false;
        }
    }

    /**
     * @param idPoste
     * @param idEquipe    : id de l’Equipe
     * @param idPersonnel : id du Personnel
     * @return : bool
     */
    @Override
    public Boolean deletePersonnelFromEquipe(Integer idEquipe, Integer idPersonnel) {
        try {
            Personnel p = personnelRepository.getOne(idPersonnel);
            Equipe e = equipeRepository.getOne(idEquipe);
            Members m = membersRepository.getMembersByEquipe_IdAndPersonnel_Id(idEquipe, idPersonnel);
            p.getEquipes().remove(m);
            e.getPersonnels().remove(m);
            membersRepository.delete(m);
            return true;

        } catch (Exception exc) {
            System.err.println(exc.getMessage());
            return false;
        }
    }

    /**
     * Modification du rôle d’un Personnel dans une Equipe
     *
     * @param role        String : role à modifier
     * @param idPersonnel Integer : id du Personnel
     * @return Members : membre modifié
     */
    @Override
    public Members updateMemberRole(String role, Integer idEquipe, Integer idPersonnel) {
        membersRepository.getMembersByEquipe_IdAndPersonnel_Id(idEquipe, idPersonnel).setRole(role);
        return membersRepository.getMembersByEquipe_IdAndPersonnel_Id(idEquipe, idPersonnel);
    }

    /**
     * Recherche d’un Membre
     *
     * @param specs    spécification recherche
     * @param pageable pagination
     * @return Page d’un Membre
     */
    @Override
    public Page<Members> searchMembres(Specification<Members> specs, Pageable pageable) {
        return membersRepository.findAll(Specification.where(specs), pageable);
    }

    /**
     * Récupération des binômes ou equipes sur le centre
     *
     * @param equipe   Bool : true for Equipe or false for Binome
     * @param pageable : Pagination
     * @return Page : Equipe
     */
    @Override
    public Page<Equipe> getEquipeByBinomeEquipe(Boolean equipe, Integer idPoste, Pageable pageable) {
        return equipeRepository.getEquipesByBinomeEquipeAndPoste(equipe, posteRepository.getOne(idPoste), pageable);
    }

    /**
     * Création d’une Equipe
     *
     * @param equipe  : Equipe à créer
     * @param idPoste Integer : id de Poste
     * @return Equipe : nouvelle Equipe
     */
    @Override
    public Equipe createEquipe(Equipe equipe, Integer idPoste) {
        return posteRepository.findById(idPoste).map(poste -> {
            equipe.setPoste(poste);
            return equipeRepository.save(equipe);
        }).orElseThrow(() -> new ResourceNotFoundException("Le Poste avec l'id " + idPoste + " non trouvé"));
    }

    /**
     * Définition du Chef d’Equipe
     *
     * @param idEquipe Integer : id de l’Equipe
     * @param idChef   Integer : id du Personnel à ajouter comme Chef d’Equipe
     * @return HttpStatus : OK si correctement effectué
     */
    @Override
    public HttpStatus defineChefEquipe(Integer idEquipe, Integer idChef) {
        Optional<Equipe> e = equipeRepository.findById(idEquipe);
        Optional<Personnel> p = personnelRepository.findById(idChef);
        if (e.isPresent() && p.isPresent()) {
            Equipe equipe = e.get();
            equipe.setChefEquipe(p.get());
            equipeRepository.save(equipe);
            return HttpStatus.OK;
        } else
            return HttpStatus.BAD_REQUEST;
    }

    /**
     * Recherche d’une Equipe
     *
     * @param specs    spécification recherche
     * @param pageable pagination
     * @return Page d’Equipe
     */
    @Override
    public Page<Equipe> searchEquipe(Specification<Equipe> specs, Pageable pageable) {
        return equipeRepository.findAll(Specification.where(specs), pageable);
    }
}
