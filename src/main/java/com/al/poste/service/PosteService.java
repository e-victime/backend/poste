package com.al.poste.service;

import com.al.poste.model.Personnel;
import com.al.poste.model.Poste;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface PosteService {
    //Admin
    Optional<Poste> createPoste(Poste Poste);
    Page<Poste> getAllPoste(Pageable pageable);
    Page<Poste> getPosteByNom(String username,Pageable pageable);
    Poste updatePostePut(Poste Poste);
    String deletePoste(Integer id);
    Poste getPosteById(Integer PosteId);
    HttpStatus changeResponsable(Integer posteId, Integer responsableId);
    Optional<Personnel> getResponsable(Integer posteId);
    //    Poste updatePostePatch(Poste Poste, Integer id);

    /**
     * Recherche d’un Poste
     * @param specs spécification recherche
     * @param pageable pagination
     * @return Page d’un Poste
     */
    Page<Poste> searchPoste(Specification<Poste> specs, Pageable pageable);
}
