package com.al.poste.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Set;

import static com.al.poste.model.Genre.*;

@Entity
@Table(name = "personnel")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter

@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
public class Personnel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "username", unique = true)
    @NonNull
    String username;
    @Column(name = "civilite")
    Genre civilite;
    @Column(name = "prenom")
    @NonNull
    String prenom;
    @Column(name = "nom")
    @NonNull
    String nom;
    @Column(name = "date_naissance")
    LocalDate dateNaissance;
    @Column(name = "actif")
    Boolean actif;
    @Column(name = "email")
    @Email(message = "Votre email doit être valide")
    String email;
    @Column(name = "password")
    String password;

    @Column(name = "phone",unique = true) @NonNull
    @Pattern(regexp = "(\\+61|0)[0-9]{9}")
    String phone;

    @OneToOne(mappedBy = "chefEquipe")
    @JsonBackReference(value = "ChefEquipeForEquipe")
    @EqualsAndHashCode.Exclude
    Equipe chefEquipe;

    @OneToMany(mappedBy = "responsable")
    @JsonIgnore
    Set<Poste> responsablePoste;

    @OneToMany(mappedBy = "personnel")
    @JsonManagedReference(value = "EquipesForPersonnel")
    @EqualsAndHashCode.Exclude
    Set<Members> equipes;

    private Personnel(
            String username,
            String civilite,
            String prenom,
            String nom,
            LocalDate dateNaissance,
            Boolean actif,
            String email
    ) {
        this.username = username;
        switch (civilite) {
            case "M":
                this.civilite = MASCULIN;
            case "Mme":
                this.civilite = FEMININ;
            case "Autre":
                this.civilite = AUTRE;
        }
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.actif = actif;
        this.email = email;
    }

    @Override
    public String toString() {

        return "Personnel{" +
                "username='" + username + '\'' +
//                ", civilite=" + civilite.toString() +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance.toString() +
                ", actif=" + actif.toString() +
                ", email='" + email + '\'' +
                '}';
    }
}