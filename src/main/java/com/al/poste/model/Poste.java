package com.al.poste.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "poste")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString(of = {
        "nom",
        "dateDebut",
        "dateFin",
        "descriptionPoste"
})
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Poste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "nom")
    String nom;

    @Column(name = "date_debut")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Paris")
    Date dateDebut;

    @Column(name = "date_fin")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Paris")
    Date dateFin;

    @Column(name = "location")
    String location;

    @Column(name = "description")
    String descriptionPoste;

    @OneToMany(mappedBy = "poste", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "EquipesForPoste")
    @EqualsAndHashCode.Exclude
    Set<Equipe> equipes;

    @ManyToOne
    @JoinColumn
    @JsonProperty("responsable")
    Personnel responsable;

    @JsonManagedReference(value = "VictimesForPoste")
    @OneToMany(mappedBy = "poste")
    @EqualsAndHashCode.Exclude
    Set<Victime> victimes;

    @ElementCollection
    List<Long> victimesIds = new ArrayList<>();
}
