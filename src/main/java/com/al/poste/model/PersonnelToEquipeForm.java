package com.al.poste.model;

import lombok.NonNull;

public class PersonnelToEquipeForm {
    @NonNull
    private Integer idPersonnel;
    @NonNull
    private String role;

    public PersonnelToEquipeForm(Integer idPersonnel, String role) {
        this.idPersonnel = idPersonnel;
        this.role = role;
    }

    public Integer getIdPersonnel() {
        return idPersonnel;
    }

    public void setIdPersonnel(Integer idPersonnel) {
        this.idPersonnel = idPersonnel;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonnelToEquipeForm)) return false;

        PersonnelToEquipeForm that = (PersonnelToEquipeForm) o;

        if (!getIdPersonnel().equals(that.getIdPersonnel())) return false;
        return getRole().equals(that.getRole());
    }

    @Override
    public int hashCode() {
        int result = getIdPersonnel().hashCode();
        result = 31 * result + getRole().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PersonnelToEquipeForm{" +
                "idPersonnel=" + idPersonnel +
                ", role='" + role + '\'' +
                '}';
    }
}
