package com.al.poste.model.bilan.Glasgow;

import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Calcul {

    @Min(value = 0)
    @Max(value = 15)
    private Integer score;

    private Calcul(@Nullable Motricite reponseMotrice,@Nullable Yeux ouvertureDesYeux,@Nullable Verbale reponseVerbale) {

        if (reponseMotrice != null) score += reponseMotrice.score();
        if (ouvertureDesYeux != null) score += ouvertureDesYeux.score();
        if (reponseVerbale != null) score += reponseVerbale.score();
    }

    public Integer getScore() {
        return score;
    }

}
