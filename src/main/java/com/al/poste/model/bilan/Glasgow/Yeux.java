package com.al.poste.model.bilan.Glasgow;

import java.util.HashMap;
import java.util.Map;

public enum Yeux implements Label {
    J("Jamais", 1),
    D("À la douleur", 2),
    B("Bruit", 3),
    S("Spontanée", 4);

    private static final Map<String, Yeux> BY_LABEL = new HashMap<>();
    private static final Map<Integer, Yeux> BY_SCORE = new HashMap<>();

    static {
        for (Yeux y : values()) {
            BY_LABEL.put(y.label, y);
            BY_SCORE.put(y.score, y);
        }
    }

    public final String label;
    public final int score;

    Yeux(String label, int score) {
        this.label = label;
        this.score = score;
    }


    public static Yeux valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static Yeux valueOfAtomicNumber(int number) {
        return BY_SCORE.get(number);
    }

    @Override
    public String label() {
        return label;
    }

    @Override
    public Integer score() {
        return score;
    }
}
