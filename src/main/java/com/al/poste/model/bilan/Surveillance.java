package com.al.poste.model.bilan;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "surveillance")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Surveillance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    EvolutionPhysique evolution;

    @Column(name = "heure", columnDefinition = "TIME")
    LocalTime heure;

    String frequenceRespiratoire, SPO2, frequenceCardiaque, tensionArterielle, echelleDouleur, temperature, glycemie;

    @ManyToOne
    @JoinColumn(name = "physique_id")
    @JsonBackReference(value = "surveillance")
    @EqualsAndHashCode.Exclude
    Physique bilan;
}
