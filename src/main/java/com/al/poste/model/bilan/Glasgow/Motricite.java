package com.al.poste.model.bilan.Glasgow;

import java.util.HashMap;
import java.util.Map;

public enum Motricite implements Label {
    ABS("Absence", 1),
    RED("Réaction en extension à la douleur", 2),
    RFD("Réaction de flexion à la douleur", 3),
    ERI("Évitement ou réponse inadaptée", 4),
    RAD("Réponse adaptée à la douleur", 5),
    OAO("Obéit aux ordres", 6);

    private static final Map<String, Motricite> BY_LABEL = new HashMap<>();
    private static final Map<Integer, Motricite> BY_SCORE = new HashMap<>();

    static {
        for (Motricite m : values()) {
            BY_LABEL.put(m.label, m);
            BY_SCORE.put(m.score, m);
        }
    }

    public final String label;
    public final int score;

    Motricite(String label, int score) {
        this.label = label;
        this.score = score;
    }
    

    public static Motricite valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static Motricite valueOfAtomicNumber(int number) {
        return BY_SCORE.get(number);
    }

    @Override
    public String label() {
        return label;
    }

    @Override
    public Integer score() {
        return score;
    }
}
