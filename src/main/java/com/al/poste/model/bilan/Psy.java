package com.al.poste.model.bilan;

import com.al.poste.model.Victime;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "bilan_psy")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Psy {
    @Id
    @Column(name = "id")
    private Long id;

    // Signes repères
    Boolean signes_agitation,
            signes_confusion,
            signes_euphorie,
            signes_mefiance,
            signes_prostration,
            signes_fuitePanique,
            signes_colere,
            signes_sideration,
            signes_culpabilite,
            signes_gestesAuto,
            signes_tristesse,
            signes_derealisation,
            signes_agressivite,
            signes_angoisse,
            signes_pleurs;

    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    Relationnel signes_contactRelationnel;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    Verbalisation signes_verbalisation;
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    Recit signes_recitEvenement;

    // Evolution
    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    EvolutionPsy evolution_etat;
    String evolution_observation;

    // Suite donnée
    Boolean suite_avisMedical, suite_avisCUMP, suite_evac, suite_hopital, suite_domicileSeul, suite_accompagne;
    LocalTime suite_heure;

    // Bilan
    Integer bilan_pouls;

    @JsonBackReference(value = "psy")
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId
    @EqualsAndHashCode.Exclude
    Victime victime;

    public Psy(Victime victime) {
        this.victime = victime;
    }
}
