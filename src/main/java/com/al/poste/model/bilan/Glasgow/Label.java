package com.al.poste.model.bilan.Glasgow;

public interface Label {
    String label();
    Integer score();
}
