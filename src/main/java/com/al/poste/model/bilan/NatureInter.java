package com.al.poste.model.bilan;

public enum NatureInter {
    Accident, Maladie, Malaise, Intoxication, Noyade, Autre
}
