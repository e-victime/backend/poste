package com.al.poste.model.bilan;

public enum CoteTrauma {
    Droit, Gauche, Deux, Avant, Arriere
}
