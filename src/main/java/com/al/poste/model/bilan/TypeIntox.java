package com.al.poste.model.bilan;

public enum TypeIntox {
    Alcool, CO, Medicament, Drogue, Alimentaire
}
