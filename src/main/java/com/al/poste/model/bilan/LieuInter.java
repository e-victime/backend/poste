package com.al.poste.model.bilan;

public enum  LieuInter {
    DPS, Public, Prive, Travail, Autre
}
