package com.al.poste.model.bilan;

public enum EvacType {
    PROTEC, SP, SMUR, PRIVE, AUTRE, HELICO
}
