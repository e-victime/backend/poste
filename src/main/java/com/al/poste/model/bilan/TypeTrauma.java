package com.al.poste.model.bilan;

public enum TypeTrauma {
    Plaie, Brulure, Hemorragie, Deformation, Douleur, FractureO, FractureF
}
