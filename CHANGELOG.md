# CHANGELOG

<!--- next entry here -->

## 1.1.3
2020-05-28

### Fixes

- **poste:** Display responsable on GET poste (8a2c61bf9a87a697c4cdcb3bf1994c134f2c439a)

## 1.1.2
2020-05-28

### Fixes

- **controller:** deprecate old search method (538d3ff11c8b9f49647cd176fac07acda04aa4e7)

## 1.1.1
2020-05-26

### Fixes

- **validation:** Add validator on email and phone number (e95ab90ca2c04a5ba663b7ba30412202ebfaa919)

## 1.1.0
2020-05-26

### Features

- **personnel:** Add phone number for firebase compatibility (0d6ae280092a9efb0b6ae4f06fc46f40a7a25832)

### Fixes

- **db:** increase timeout time (ef6b8c6c4f2e8afa23b38d1a7a863abf685b0be8)

## 1.0.13
2020-05-26

### Fixes

- **discovery:** Change zone URL (45100e15c0eeddb1ee4780a2a87180fdb002e359)

## 1.0.12
2020-05-25

### Fixes

- **discovery:** Fix missing port on Eureka URL (8ca2b62387d69e527f08b03c0412c87f52c43ea4)

## 1.0.11
2020-05-25

### Fixes

- **env vars:** Change Eureka env var (701f1f5f3a49bdf1d7db9801b8e9d80be99c08e9)

## 1.0.10
2020-05-19

### Fixes

- **versioning:** Repair versioning in POM (6d5a968489329e6a871818040991ddceb07a5a82)

## 1.0.9
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (03ed872c786f2e667773ddb5ad13c3e66c9bab4b)

## 1.0.8
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (b81f582d02c538dc35475ba7e1acbc48dc8a364f)
- **cors:** CORS for swagger ui (dc565633e8af020367f4fd01967b4917404d081f)

## 1.0.7
2020-05-11

### Fixes

- **monitoring:** Expose all actuator routes (d3dbfe96c5e59677547f4f57b94c8e0f07f5bf65)

## 1.0.6
2020-05-11

### Fixes

- **controller:** Clean controller routes (59992163676ed5695d5e6f3ac23333fadd74ae60)

## 1.0.5
2020-05-10

### Fixes

- **recursion:** repair json recursion (15ec8e4bea8b3f1c4de363850bf2e4466e2327af)

## 1.0.4
2020-05-09

### Fixes

- **pom:** Update dependencies (31afed4d852986e66446bd1d3ac2dd5b616587b3)

## 1.0.3
2020-05-09

### Fixes

- **config:** Add discovery config (521a452060fd9807a9d25712beef4c92a4dbb1f5)
- **deployment:** Change registry url (48c958078b7e79171769cef195cdc28cc508a45a)

## 1.0.2
2020-05-08

### Fixes

- **var:** Update env variables (2cc6d81e75607908e51562e1e39f8243412eaaa6)

## 1.0.1
2020-05-08

### Fixes

- **spring boot:** Update Spring Boot (eee6c09be389d03d555abba6dfb0d925e7ca1662)

## 1.0.0
2020-05-03

### Fixes

- **ci:** Add versionning in pom file (b758c5949185350a3326e9331e2f6ed8a1cfc0bc)

## 0.12.4
2020-04-14

### Fixes

- **victimemodels:** Infinite recursion in victime hashcode. (59d0f0cf4212785a3a76ea41092a456290f94bf2)

## 0.12.3
2020-04-14

### Fixes

- **prod:** Change db timeout (b206924b3989f180779baf1fd2002509c11731a1)

## 0.12.2
2020-04-13

### Fixes

- **deployment:** Update running deployment with ci (e92caa517055543de54c63f5ead762d214af05e1)

## 0.12.1
2020-04-13

### Fixes

- **models:** Hashes were recusively the same. (60f12385396da3881fcec27927be12c5e47a1208)
- **deletepersonnelfromequipe:** idPersonnel was used to find an equipe. (7c0acd8fb656d32919ae2d7a8c0822d30fbe965d)

## 0.12.0
2020-04-12

### Features

- **bilan:** Add Surveillance with relation on Physique (8f09fc9799c49a99641e268ad87f417cb1b5d074)

### Fixes

- **todo:** Delete TODO comment (837a8a76492c9539040e5fa0bc4deb8ffa511251)
- **date:** Define json date pattern (89a887eaf696ff1c9abc5714a9f95abe1327f297)

## 0.11.3
2020-04-11

### Fixes

- **controller:** delete TODO (57b67b14365ca0400e35453df63e096aa76778be)

## 0.11.2
2020-04-11

### Fixes

- **controller:** Add personnel to Equipe (ec20c0c39e89cff9fdf601e37700abe5f1362f76)

## 0.11.1
2020-04-11

### Fixes

- **controller:** fix arguments for add personnel to equipe (6dd5943367b2ec6d7f5199526632bbeaf2774845)

## 0.11.0
2020-04-10

### Features

- **deployment:** Update image on ci (5a6608452f680118ac3c4562cdc1e4e1e026d83c)
- **model:** Update victime's models (4af700812ff4dceed926c36aed1a8a78a35143dd)

## 0.10.1
2020-04-09

### Fixes

- Repair some stuff (9c79cde7226e02ddcf441ec1799b828a6b475f93)

## 0.10.0
2020-04-08

### Features

- delete centre (d8d0ee47a428994981e58ea9cbcec826f6721628)

## 0.9.0
2020-04-08

### Features

- **responsable:** Add get Responsable (6d80149a05461d00b7a45fe67f64b8e6a6901ce1)

## 0.8.0
2020-04-08

### Features

- **responsable:** Add get Responsable (b110fa610d321604e4b6400b62bce5729a5d8b26)

## 0.7.0
2020-04-07

### Features

- Add search method (58c069a3c2fbabe34f87a47a6e41b87fcd46ebc5)

## 0.6.4
2020-04-07

### Fixes

- **postemodel:** change date format to fit with form datetimelocal (7b70ea3977d02757c5e883a3d5139c7401021a6c)
- **postemodel:** taking back what I said :) (f558e959070921f1b69f0c21c7234a784544c61a)

## 0.6.3
2020-04-07

### Fixes

- **models:** Define name to all references and changed sides of managed and backreferences (9975a87b2f880f9e90964655c3487d425799ee2e)

## 0.6.2
2020-04-07

### Fixes

- **models:** deleted JSON Management and back references + LocalDateTime to Date in Poste Model (330e3bf80e05c6a069fa6a43952f9a0d056bf8cb)
- **postemodel:** error DateTime don't exists changed to Date (fb57468ab49fabed5dbd15052a2d0bd210aec1b4)

## 0.6.1
2020-04-06

### Fixes

- **controller:** Add RequestBody) (ee6f2df4542246386b82c3a05e9760c20ceb0ed0)

## 0.6.0
2020-04-06

### Features

- **personnel:** Add password (6fac0db4807cf2f9c2cd79e9f55cf6b70626e5cf)

### Fixes

- **cors:** Add cors filter (d4f9872614d9452ee66f68e95f18361e962c87fe)

## 0.5.1
2020-04-05

### Fixes

- **id:** Change generation type (fcb2bc4c5a84c87cd5d8464fc73d9f001ef1e64d)

## 0.5.0
2020-04-05

### Features

- **doc:** Add springdoc (bc655b1e5b40918621a556cd8d3488ab060d039d)

## 0.4.2
2020-04-04

### Fixes

- **serialization:** fixing infinite loop during Serialization (0e141c54bfd76df29d9d05065f6821e000579693)

## 0.4.1
2020-04-03

### Fixes

- **ci:** Update ci (e71ac3498e682b1eaeee341ff82265e1207317ca)

## 0.4.0
2020-04-02

### Features

- **controller:** Add method to change reponsable (2495fed8a36f32a0d54262130a9158232f7f8274)

## 0.3.0
2020-04-02

### Features

- **controler:** Add Personnel in Equipe routes (724f3480cda35fcdb2db11e3bfc7f50fefe30e07)

## 0.2.0
2020-03-31

### Features

- **equipe/personnel:** Add Members (087090fc9e46a94254b6b14b18582013961d6fcd)
- **service:** Add delete method on Centre (d5ac9a4911f1fd57ce4a05aa67d3cd41f4dfbffe)
- **services:** Update role in Equipe method && find all Equipe on Poste (1a017faf3bfbaddeade9f2e6e6fa9781ce16ed8e)
- **depencies:** Add jetbrains's dependencies (c5fe0ae3c47290a9d60935a66092d99a7454c5e6)
- **controler:** Add Centre and Equipe routes (62dc5b0655694c88422413457897887cd703c9f0)

### Fixes

- **model:** Adjuste Models from Personnel micro-service (1046710dc7bdd3dd86b867036bdf134b124478ba)
- **deployment:** use latest image (639a90b0c51a7dd5cf7570697bfe249b5b718ae5)
- **relations:** Relation Poste/Personnel (102b0669a7cc27ed18a0e909a9c1b802089f4cd4)
- **import:** Optimize imports (b27423a80b1794c29163a9eebbcb170d552c854a)
- **repository:** Rename Service (dc77da2157e71586c6d976969aa630a4105dc8b2)
- **relation:** Correct relation Centre/Equipe and Add createEquipe in Service (fde41e02c72b1456f9af26b416c98523cc1ed69b)
- **imports:** Remove unused imports (7ad1b712a9ee4c16dcbf31c239aa25d813157918)

## 0.1.2
2020-03-24

### Fixes

- **deployment:** Add namespace in deployment (94dd5ae2150cef08d69bf0d910c7748cc20a2d17)

## 0.1.1
2020-03-24

### Fixes

- **ci:** Change image for version stage (83f07a822bd44f3e99d744de590cfff1d762c799)

## 0.1.0
2020-03-24

### Features

- **init:** init submodule (82698cfdce4062b4f8d2bca9140d2601fd808a95)
- **model:** Add models (6f927d450bc050c9dfe563e4684a967620e1e10c)
- **ci:** Prepare micro-service for CI (0b72b44129c7042be8881663431f8f29a173cf30)
- **model:** Edit models for datasource and repair relation between Equipe, Centre and Poste (e262a860aadab733e73b68c19476bac18748bce7)
- Add files for micro-service initialisation (c9a55e8f8f963e41b368256638f798be74068cb7)
- **repository:** Prepare Poste repositories (e41798d20c59d46329b40cc413310351c247130e)

### Fixes

- **repository:** Use Id instead object when possible and repair query in CentreRepository (1ff7f4bb545610feeea0ce35cff4ccff82518ea7)